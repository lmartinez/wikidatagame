# Creación de cuentas para participar
Para participar en el concurso [Desafío de programación "Juegos en Wikidata"](https://www.wikidata.org/wiki/Wikidata_talk:Desaf%C3%ADo_de_programaci%C3%B3n_%22Juegos_en_Wikidata%22) es necesario contar con las siguientes cuentas:
- Cuenta de MediaWiki [Crear cuenta mediawiki](./crearCuentaMediawiki.md)
- Cuenta de desarrollador de Wikimedia [Crear Wikimedia developer account](crearWikimediaDeveloperAccount.md)

## Referencias
https://wikitech.wikimedia.org/wiki/Portal:Toolforge/Quickstart