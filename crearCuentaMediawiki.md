# Crear cuenta de usuario en MediaWiki
1. Ingresar al sitio [Crear cuenta MediaWiki](https://www.mediawiki.org/w/index.php?title=Special:CreateAccount)
2. Llenar el siguiente formulario:

 ![Creación de cuenta](imagenes/cuentamediawiki_01.png)

3. Luego de completar el formulario tendrá una cuenta de MediaWiki.
