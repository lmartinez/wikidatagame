# Crear cuenta de desarrollador de WikiMedia

1. Ingresar a la página de la consola administración de ToolForge [Toolforge admin console](https://toolsadmin.wikimedia.org/register/)

2. Como **Toolforge** utiliza el protocolo *OAuth* para autenticar a los usuarios, solicita el acceso con la cuenta de **Wikimedia** creada en la sección anterior.

Presionar el botón **Login using Wikimdia account**

![Creación de cuenta](imagenes/cuentamediawikideveloper_01.png)

3. Si en el navegador de internet no hay una sesión de usuario Wikimedia iniciada, se presentará el siguiente formulario para identificarse con la cuenta de **Wikimedia**:

![Creación de cuenta](imagenes/cuentamediawikideveloper_02.png)

4. Luego de identificarse o en el caso de haber tenido una sesión iniciada en **Wikimedia**, el sitio de **ToolForge** solicitará permiso para acceder a la cuenta de **Wikimedia** y presentará la pregunta siguiente:

![Creación de cuenta](imagenes/cuentamediawikideveloper_03.png)

Presionar el botón **Allow**.

5. El paso siguiente consiste en seleccionar el nombre que se usará como desarrollador. Se puede utilizar el mismo nombre de la cuenta de **Wikimedia** o el nombre real (ej. "Juan Pérez")

![Creación de cuenta](imagenes/cuentamediawikideveloper_04.png)

Por defecto se coloca el nombre de la cuenta de **Wikimedia**, pero se puede reemplazar. El recuadro y la tilde en color verde indican que el nombre elegido está disponible.

Presionar el botón **Next**.

6. A continuación se debe elegir el nombre de usuario para utilizar en la consola de UNIX. Debe iniciar con un caracter alfabético del rango `a-z` y sólo puede contener minúsculas (`a-z`), digitos (`0-9` y el símbolo `-`)

![Creación de cuenta](imagenes/cuentamediawikideveloper_05.png)

Por defecto se coloca el nombre elegido en el paso anterior (adaptado a los requisitos), pero se puede reemplazar. El recuadro y la tilde en color verde indican que el nombre elegido está disponible.

Presionar el botón **Next**.

7. Para continuar con el registro, se debe ingresasr una cuenta de correo electrónico válida.

![Creación de cuenta](imagenes/cuentamediawikideveloper_06.png)

Presionar el botón **Next**.

8. Luego configurar la contraseña ingresándola en ambos campos.

![Creación de cuenta](imagenes/cuentamediawikideveloper_07.png)

Presionar el botón **Next**.

9. El último paso consiste en verificar y confirmar los datos de la cuenta que ese está creando. Para ello se debe marcar la casilla aceptando los términos de uso y código de conducta.

![Creación de cuenta](imagenes/cuentamediawikideveloper_08.png)

Presionar el botón **Create account**.

10. Si todo está correcto, La siguiente pantalla presenta un mensaje confirmando que se creó la cuenta y solicita el inicio de sesión para continuar.

![Creación de cuenta](imagenes/cuentamediawikideveloper_09.png)



## Referencias

[(Crear Wikimedia developer account)](https://wikitech.wikimedia.org/wiki/Help:Create_a_Wikimedia_developer_account)
